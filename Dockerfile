#Download base image ubuntu 16.04
FROM ubuntu:18.04

# Update Ubuntu Software repository
RUN apt-get update

RUN apt-get -y install mosquitto curl gnupg git
RUN curl -sL https://deb.nodesource.com/setup_10.x  | bash -
RUN apt-get -y install nodejs

VOLUME "./:/var/www/server/"

WORKDIR /var/www/server/

COPY . .

CMD "./docker/server/startup.sh"

EXPOSE 3000 1883
