import * as _ from "underscore";
import io from "socket.io-client";
import he from "he";
import {toHHMMSS} from "./helpers";
import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";
import {machinesList} from "./components/machines-list";
import {machineDetails} from "./components/machine-details";

Vue.use(VueAxios, axios);

String.prototype.toHHMMSS = toHHMMSS;

let app = new Vue({
  delimiters: ["${", "}"],
  data() {
    return {
      window,
      io,
      socket: null,
      devices: this.setInitObjFromStr(window.machinesInitialString)
    };
  },
  components: {
    "machines-list": machinesList,
    "machine-details": machineDetails
  },
  mounted() {
    this.socketConn();
  },
  methods: {
    setInitObjFromStr(str) {
      return str.length > 0 ? _.indexBy(JSON.parse(he.decode(str)), "id") : {};
    },
    socketConn() {
      this.socket = io.connect("http://localhost:3000");
      this.socket.on("update", (newObj) => {
        this.devices[newObj.id] = _.extendOwn(
          this.devices[newObj.id],
          newObj.update
        );
      });
      this.socket.on("add", (newObj) =>
        this.$set(this.devices, newObj.id, newObj)
      );
      this.socket.on("remove", (id) => this.$delete(this.devices, id));
    }
  }
}).$mount("#app");
