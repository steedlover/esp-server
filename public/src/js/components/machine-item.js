export const machineItem = {
  template: `
    <div>
      <div class="devices-list-item-header">
        <span v-if="device.error_text" class="message-error">{{device.error_text}}</span>
        <span v-else class="message fas fa-microchip"></span>
        <span class="fas fa-wifi"
          :class="{'icon-online': device.status > 0, 'icon-offline': device.status <= 0}">
        </span>
        </div>
        <div class="devices-list-item-body">
          <ul class="props-list">
            <li class="props-list-item"><b>ID:</b> {{device.id}}</li>
            <li class="props-list-item"><b>Type:</b> {{device.type}}</li>
            <li class="props-list-item"><b>Uptime:</b> {{getStringTime(device.timestamp)}}</li>
            <li class="props-list-item">
              <b>Current state:</b>
              <span v-if="device.state === 'on'" class="fas fa-lightbulb"></span>
              <span v-if="device.state === 'off'" class="far fa-lightbulb"></span>
            </li>
            <li class="props-list-item"><a :href="getDeviceLink(device.id)">[ Show details ]</a></li>
          </ul>
          <div class="devices-list-item-signals" v-if="device.signals">
            <ul class="list-plain">
              <li v-if="device.status > 0">
                <a class="button"
                  :class="{'button-primary': index === 0, 'm-right-05': index + 1 !== device.signals.length}"
                  v-for="(sig, index) in device.signals" @click="sendSignal(device.id, sig)"
                >
                  {{sig}}
                  <span v-if="sig === 'on' || sig === 'off'"
                    class="icon fa-sun" :class="{'fas': sig === 'on', 'far': sig === 'off'}"
                  ></span>
                </a>
                <a class="button button-danger" @click="sendSignal(device.id, 'reset')">
                  Reset
                  <span class="icon fas fa-power-off"></span>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  `,
  props: ["device"],
  data() {
    return {
      interval: null
    };
  },
  watch: {
    "device.status": function(newVal, oldVal) {
      if (newVal > 0 && oldVal <= 0) {
        // Device is back online
        this.startTimer();
      } else if (oldVal > 0 && newVal <= 0) {
        // Device goes offline
        this.stopTimer();
      }
    }
  },
  mounted() {
    if (this.device.status > 0) {
      this.startTimer();
    }
  },
  methods: {
    getDeviceLink(id) {
      return "/machines/" + id + "/";
    },
    getStringTime(str) {
      return str.toString().toHHMMSS();
    },
    startTimer() {
      this.interval = setInterval(() => this.device.timestamp++, 1000);
    },
    stopTimer() {
      if (this.interval) {
        clearInterval(this.interval);
      }
    },
    sendSignal(devid, sig) {
      this.axios
        .post("http://localhost:3000/api/machineSend", {
          id: devid,
          signal: sig
        })
        .catch((e) => console.warn(e));
    }
  }
};
