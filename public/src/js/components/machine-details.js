import VJsoneditor from "v-jsoneditor";
import he from "he";
import {keys, size} from "underscore";

export const machineDetails = {
  components: {
    VJsoneditor
  },
  template: `
    <div>
      <div v-if="isMachineAccessible()">
        <div class="row">
          <div class="col-12">
            <ul class="list-plain">
              <li><b>ID:</b> {{id}}</li>
              <li>
                <b>Status:</b>
                <span class="message-error" v-show="device.status <= 0">Offline</span>
                <span class="message" v-show="device.status > 0">Online</span>
              </li>
            </ul>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <v-jsoneditor v-model="json"
              :key="editorKey"
              :options="options"
              :plus="false" height="400px"
              @error="onError"
            ></v-jsoneditor>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <a class="button button-primary"
              :class="{'disabled': !isConfigChanged() || isDeviceOffline()}"
              :tabindex="!isConfigChanged() ? '-1' : null"
              @click="saveConfigChanges()"
            >
              <span class="icon fas fa-file-export m-right-05"></span>
              Apply changes
            </a>
          </div>
        </div>
      </div>
      <div v-else>
        <div class="row">
          <div class="col-12">The device is not accessible</div>
        </div>
      </div>
    </div>
  `,
  props: ["machines"],
  data() {
    return {
      json: {},
      id: "",
      editorKey: 1,
      device: {},
      options: {
        mode: "code",
        onEditable: () => {
          return !this.isDeviceOffline();
        }
      }
    };
  },
  watch: {
    "device.status": function(newVal, oldVal) {
      if ((newVal <= 0 && oldVal > 0) || (newVal > 0 && oldVal <= 0)) {
        this.editorKey++;
      }
    },
    machines: {
      handler: function(newVal) {
        if (newVal[this.id] && size(newVal[this.id]) > 0) {
          this.device = newVal[this.id];
          this.editorKey++;
        } else {
          this.device = {};
        }
      },
      deep: true
    }
  },
  mounted() {
    if (size(this.machines) > 0) {
      this.id = keys(this.machines)[0];
    }
    this.device = this.machines[this.id];
    if (this.device.source && typeof this.device.source === "string") {
      this.json = JSON.parse(he.decode(this.device.source));
    }
  },
  methods: {
    isDeviceOffline() {
      return this.device.status <= 0 ? true : false;
    },
    isMachineAccessible() {
      const accessible = size(this.device) > 0 ? true : false;
      if (this.id && !accessible) {
        setTimeout(() => (location.href = "/machines"), 2000);
      }
      return accessible;
    },
    saveConfigChanges() {
      if (this.isConfigChanged()) {
        this.axios
          .post("http://localhost:3000/api/machineSend", {
            id: this.id,
            signal: "new_config",
            arg: JSON.stringify(this.json, null, 4)
          })
          .then((res) => {
            try {
              if (res.data.status !== 200 || res.data.message !== "success") {
                throw "Unrecognised response on saveConfigChanges";
              }
            } catch (e) {
              throw e;
            }
          })
          .catch((e) => console.warn(e));
      }
    },
    isConfigChanged() {
      const newConfEncoded = he.encode(JSON.stringify(this.json));
      return newConfEncoded === this.device.source ? false : true;
    },
    onError() {
      console.log("error");
    }
  }
};
