import {machineItem} from "./machine-item";

export const machinesList = {
  components: {
    machine: machineItem
  },
  template: `
    <div>
      <div v-if="Object.keys(machines).length > 0">
        <div class="devices-list-item col-6" v-for="device in machines" :key="device.id">
          <machine :device="device"></machine>
        </div>
      </div>
      <div v-else class="col-12">There are no registered devices</div>
    </div>
  `,
  props: ["machines"]
};
