import fs from "fs";
import logger from "../service/logger";

class ReadConfig {
  constructor() {
    this.data = {};
    this.file = "";
    this.rootPath = "config";
  }

  setFilename(file) {
    if (typeof file === "string") {
      this.file = file;
    }
    return this;
  }

  getData(type) {
    if (!this.file) {
      return;
    }
    try {
      let filedata = this.getFileData(this.file);
      if (!filedata) {
        throw "config file is unavailable: " + this.file;
      }
      if (!this.data[this.file]) {
        this.data[this.file] = filedata;
      }
      if (type) {
        return this.data[this.file][type]
          ? this.data[this.file][type]
          : undefined;
      } else {
        return this.data[this.file] ? this.data[this.file] : undefined;
      }
    } catch (e) {
      logger.error(e);
    }
  }

  getFileData(file) {
    if (typeof file !== "string" || !file) {
      return;
    }
    if (this.data[file]) {
      return this.data[file];
    } else {
      return this.readFile(file);
    }
  }

  readFile(file) {
    try {
      let datarow = fs.readFileSync(this.rootPath + "/" + file + ".json");
      return JSON.parse(datarow);
    } catch (e) {
      logger.error("error opening config file. " + file);
      return;
    }
  }
}

class Config {
  constructor() {
    if (!Config.instance) {
      Config.instance = new ReadConfig();
    }
    return Config.instance;
  }
}

export default Config;
