import {applyMiddleware, createStore, combineReducers} from "redux";
import {extend as objectMerge} from "underscore";
import moment from "moment";
import logger from "../service/logger";
import {getHtmlMachine} from "../service/machine";
import socket from "../lib/socket-server";

const io = new socket().getInstance();

function socketNotifier({getState}) {
  return (next) => (action) => {
    // Do stuff after the state is being changed
    next(action);

    switch (action.type) {
      case "ADD_MACHINE":
        // send to socket simplyfied object
        io.sendAddDevice(getHtmlMachine(action.payload.device.id));
        break;
      case "UPDATE_STATE_MACHINE":
      case "MACHINE_GOES_OFFLINE":
        io.sendUpdateDevice(action.payload);
        break;
      case "REMOVE_MACHINE":
        io.sendRemoveDevice(action.payload.id);
        break;
    }
  };
}

function machines(state = {}, action = {}) {
  switch (action.type) {
    case "ADD_MACHINE":
      try {
        objectMerge(action.payload.device, {
          timestamp: 4,
          registered: moment().format(),
          updated: moment().format()
        });
        state[action.payload.id] = action.payload.device;
      } catch (e) {
        logger.error(
          "error on adding device: " + JSON.stringify(action.payload)
        );
      }
      return state;
    case "REMOVE_MACHINE":
      try {
        delete state[action.payload.id];
      } catch (e) {
        logger.error(
          "error on removing device: " + JSON.stringify(action.payload)
        );
      }
      return state;
    case "UPDATE_MACHINE":
      try {
        objectMerge(state[action.payload.id], action.payload.update, {
          updated: moment().format()
        });
      } catch (e) {
        logger.error(
          "error on updating device: " + JSON.stringify(action.payload)
        );
      }
      return state;
    case "UPDATE_STATE_MACHINE":
      try {
        action.payload.update = objectMerge(action.payload.update, {
          updated: moment().format()
        });
        objectMerge(state[action.payload.id], action.payload.update);
      } catch (e) {
        logger.error(
          "error on updating state of the device: " +
            JSON.stringify(action.payload)
        );
      }
      return state;
    case "MACHINE_GOES_OFFLINE":
      try {
        action.payload.update = objectMerge(action.payload.update, {
          updated: moment().format()
        });
        objectMerge(state[action.payload.id], action.payload.update);
      } catch (e) {
        logger.error(
          "error on updating state of the device: " +
            JSON.stringify(action.payload)
        );
      }
      return state;
    default:
      return state;
  }
}

const reducers = combineReducers({
  machines
});
const State = createStore(reducers, applyMiddleware(socketNotifier));

export {State};
