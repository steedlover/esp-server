import {State as state} from "./state";
import {extend as objectMerge} from "underscore";
import logger from "../service/logger";

function prepareMachinesList(list) {
  return Object.keys(list).map((key) => {
    // Copy of the object to prevent changing the parent
    let el = objectMerge({}, list[key]);
    if (el.mqtt) {
      delete el.mqtt;
    }
    if (el.confirmObserver) {
      delete el.confirmObserver;
    }
    if (el.pingObserver) {
      delete el.pingObserver;
    }
    return el;
  });
}

function getState(id, html) {
  const machines = state.getState().machines;
  html = typeof html !== "boolean" ? true : html;

  if (id) {
    try {
      const m = machines[id],
        sendObj = {};
      if (!m || !m.id) {
        throw "Machine is not registered. ";
      }
      sendObj[m.id] = m;
      return html ? prepareMachinesList(sendObj)[0] : m;
    } catch (e) {
      logger.error(e + "ID: " + id);
      return;
    }
  }

  return prepareMachinesList(machines);
}

export function getMachineList() {
  return getState();
}

export function getHtmlMachine(id) {
  return getState(id);
}

export function getMachineInstance(id) {
  return getState(id, false);
}
