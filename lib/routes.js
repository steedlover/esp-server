import path from "path";
import fs from "fs";
import express from "express";
import {capitalize} from "../lib/extra/string";
import bodyParser from "body-parser";

export default function(parent, options) {
  let dir = path.join(__dirname, "..", "controllers");
  const excludeMethods = ["name", "prefix", "before", "methods"];

  // Read all folders inside the controllers
  // except the api folder
  fs.readdirSync(dir)
    .filter((folder) => folder !== "api")
    .forEach((name) => {
      const file = path.join(dir, name),
        ext = path.extname(file).substring(1),
        verbose = options.verbose;

      const obj = require(file);

      name = obj.name || name;
      let prefix = obj.prefix || "",
        app = express(),
        handler,
        method,
        url;

      // generate routes based
      // on the exported methods
      for (var key in obj) {
        // "reserved" exports
        if (~excludeMethods.indexOf(key)) continue;
        // route exports
        switch (key) {
          case "index":
            method = "get";
            url = "/";
            break;

          case "show":
            method = "get";
            url = "/" + name + "s/:" + name + "_id";
            break;

          case "list":
            method = "get";
            url = "/" + name + "s";
            break;

          default:
            throw new Error("unrecognized route: " + name + "." + key);
        }

        // setup
        handler = obj[key];
        url = prefix + url;

        // before middleware support
        if (obj.before) {
          app[method](url, obj.before, handler);
          console.log(
            "   %s %s -> before -> %s",
            method.toUpperCase(),
            url,
            key
          );
        } else {
          app[method](url, handler);
          console.log("   %s %s -> %s", method.toUpperCase(), url, key);
        }
      }

      // mount the app
      parent.use(app);
    });

  // Read the API controllers folder
  let apiDir = path.join(__dirname, "..", "controllers/api");
  fs.readdirSync(apiDir)
    .filter(
      (name) => path.extname(path.join(apiDir, name)).substring(1) === "js"
    )
    .forEach((name) => {
      const file = path.join(apiDir, name),
        fname = name.split(".")[0];

      const obj = require(file);

      let prefix = obj.prefix || "",
        app = express(),
        handler,
        method,
        methods = obj.methods || {},
        url;

      // parse application/x-www-form-urlencoded
      //app.use(bodyParser.urlencoded({ extended: false }))

      // parse application/json
      app.use(bodyParser.json());

      // generate routes based
      // on the exported methods
      for (var key in obj) {
        // "reserved" exports
        if (~excludeMethods.indexOf(key)) continue;

        try {
          // setup
          method = methods[key] || "get";
          handler = obj[key];
          url = prefix + "/" + fname + capitalize(key);

          // before middleware support
          if (obj.before) {
            app[method](url, obj.before, handler);
            console.log(
              "   %s %s -> before -> %s",
              method.toUpperCase(),
              url,
              key
            );
          } else {
            app[method](url, handler);
            console.log("   %s %s -> %s", method.toUpperCase(), url, key);
          }
        } catch (e) {
          throw "unrecognized route: " + name + "." + key;
        }
      }

      // mount the app
      parent.use(app);
    });
}
