import mqtt from 'mqtt';
import State from '../service/state';

class Mqtt {

  constructor() {
    this.client = null;
    this.listeners = {};
    this.host = 'mqtt://localhost:1883';
  }

  connect() {
    return new Promise((resolve, reject) => {
      this.client  = mqtt.connect(this.host);

      this.client.on('error', err => {
        this.client.end();
        reject(err);
      });

      this.client.on('connect', () => {
        this.listen();
        resolve();
      });
    });
  }

  listen() {
    this.client.on('message', (topic, message) => {
      // Read string by default
      let msg = message.toString();
      // If there are curly brackets, parse the string to object
      if (message.indexOf('{') >= 0 && message.indexOf('}') >= 0) {
        msg = JSON.parse(msg);
      }
      this.gotMessage(topic, msg);
    });
  }

  addListener(topic, observer) {
    this.client.subscribe(topic);
    this.listeners[topic] = observer;
  }

  gotMessage(topic, message) {
    if (this.listeners[topic]) {
      this.listeners[topic].next(message);
    }
  }

  send(topic, message) {
    this.client.publish(topic, message);
  }

}

class MqttClient {

  constructor() {
    if (!MqttClient.instance) {
      MqttClient.instance = new Mqtt();
    }
  }

  getInstance() {
    return MqttClient.instance;
  }

}

export default MqttClient;

