import io from "socket.io";

class Socket {
  constructor() {}

  start(server) {
    this.socket = io(server);

    this.socket.on("connection", (socket) => {
      console.log("socket connected");
    });

    this.sendSocket = (type) => (data) => {
      this.socket.emit(type, data);
    };

    this.sendUpdate = this.sendSocket("update");

    this.sendAdd = this.sendSocket("add");

    this.sendRemove = this.sendSocket("remove");
  }

  sendUpdateDevice(data) {
    return this.sendUpdate(data);
  }

  sendAddDevice(data) {
    return this.sendAdd(data);
  }

  sendRemoveDevice(data) {
    return this.sendRemove(data);
  }
}

class SocketClient {
  constructor() {
    if (!SocketClient.instance) {
      SocketClient.instance = new Socket();
    }
  }

  getInstance() {
    return SocketClient.instance;
  }
}

export default SocketClient;
