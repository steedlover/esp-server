#!/bin/sh

if [ ! -d "/var/www/server/node_modules" ] || [ ! "$(ls -a /var/www/server/node_modules)" ]; then
  rm -rf /var/www/server/node_modules
  cd /var/www/server
  npm install
  npm run build:public
fi

# Start mosquitto server
service mosquitto start

# Build static files
npm run build:public
# Start application
npm start
