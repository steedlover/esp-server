The MQTT server for serving ESP8266 controllers

Installation of the mosquito server is mandatory to make it work

# Start docker version
```
docker pull dimoz/micro-server:latest

docker run -p 3000:3000 -p 1883:1883 --detach --name <container-name> dimoz/micro-server:latest
```

container-name could be any, also don't forget to turn off firewall!!!

if during the installation the error occurs:
```Docker Desktop requires Windows 10 Pro or Enterprise version 15063 to run```

[This article](https://itnext.io/install-docker-on-windows-10-home-d8e621997c1d) has worked for me

After the container has been started, the application will be available in browser with the url: http://localhost:3000

# Install MOSQUITTO server for Linux users:
```
sudo apt-get install mosquitto

sudo service mosquitto start
```

# Install the project
Install all dependencies:

```
npm install

npm run build:public
```

## Start the project
```npm start```

## Start development version
```npm start:dev```
