import {State as state} from "../../service/state";
import {extend as objectMerge} from "underscore";
import logger from "../../service/logger";
import {
  getMachineList,
  getHtmlMachine,
  getMachineInstance
} from "../../service/machine";

export const prefix = "/api";

export const methods = {
  show: "post",
  send: "post"
};

export function list(req, res, next) {
  //res.render('list', {title: 'machines', description: 'just the list of the machines', layout: false});
  try {
    const machines = getMachineList();

    if (typeof machines !== "object" || machines.length === undefined) {
      throw {message: "Can't retrieve list of the machines"};
    }

    return res.status(200).json({
      status: 200,
      data: machines,
      message: "Succesfully Machines Retrieved"
    });
  } catch (e) {
    return res.status(400).json({status: 400, message: e.message});
  }
}

export function show(req, res, next) {
  try {
    const machine = getHtmlMachine(req.machine_id);

    if (!machine) {
      throw {message: "Can't get the machine"};
    }

    return res.status(200).json({
      status: 200,
      data: machine,
      message: "Succesfully Machine Retrieved"
    });
  } catch (e) {
    return res.status(400).json({status: 400, message: e.message});
  }
}

export function send(req, res, next) {
  const id = req.body.id,
    signal = req.body.signal,
    arg = req.body.arg;
  try {
    getMachineInstance(id).sendMqttMessage(signal, arg);
    return res.status(200).json({status: 200, message: "success"});
  } catch (e) {
    logger.error(
      "id: " +
        id +
        ", signal: " +
        signal +
        ", argument: " +
        arg +
        " error sending signal: " +
        e
    );
  }
}
