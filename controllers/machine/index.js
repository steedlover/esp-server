import logger from "../../service/logger";
import {getMachineList, getHtmlMachine} from "../../service/machine";

export function list(req, res, next) {
  const list = getMachineList();

  try {
    if (typeof list !== "object" || list.length === undefined) {
      throw {message: "Can't retrieve list of the machines"};
    }
    return res.status(200).render("machine/list", {
      title: "Devices List",
      machines: JSON.stringify(list),
      layout: "default"
    });
  } catch (e) {
    return res
      .status(500)
      .render("5xx", {message: e.message, layout: "default"});
  }
}

export function show(req, res, next) {
  try {
    const machine = getHtmlMachine(req.params.machine_id);

    if (machine.length === 0) {
      throw {message: "404"};
    }
    return res.status(200).render("machine/show", {
      title: "Device info",
      machine: JSON.stringify([machine]),
      layout: "default"
    });
  } catch (e) {
    if (e.message === "404") {
      return res.redirect("/machines");
    }
    return res
      .status(500)
      .render("5xx", {message: e.message, layout: "default"});
  }
}
