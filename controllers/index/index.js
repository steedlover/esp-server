export function index(req, res, next) {
  res.render('index', { layout: 'default', title: 'Welcome!' });
}
