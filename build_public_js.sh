#!/bin/sh
pubsrcjs='public/src/js/';
pubdestjs='public/static/js/'
entrypoint='app.js';
bundlejs='bundle.js'
bundleminjs='bundle.min.js'
env='production'

### Check and if needed create the JS folder
if [ -d $pubdestjs ]; then
  echo "Using \"$pubdestjs\" folder"
  rm -rf "$pubdestjs"*
else
  mkdir -p "$pubdestjs"
  echo "\"$pubdestjs\" folder has been created"
fi

if [ -n $1 ]; then
  param="$(echo $1 | cut -d'=' -f1)"
  value="$(echo $1 | cut -d'=' -f2)"

  if [ ! -z "$param" -a "$param" = "env" ]; then
    if [ ! -z "$value" -a "$value" = "dev" ]; then
      env="development"
    fi
  fi
fi

npx browserify $pubsrcjs$entrypoint \
  -o "$pubdestjs$bundlejs" -d -t \
  [ babelify \
    --presets [ @babel/preset-env ] \
  ]

echo "The \"$pubdestjs$bundlejs\" has been compiled"

### Minify the bundle file
### if the environment is production
if [ $env = "production" ]; then
  npx node-minify --compressor babel-minify \
    --input "$pubdestjs$bundlejs" \
    --output "$pubdestjs$bundleminjs" \
    --option '{"warnings": true, "mangle": true}'
fi

### If the environment is development
### the bundle file need to be renamed
if [ $env = "development" ]; then
  cp "$pubdestjs$bundlejs" "$pubdestjs$bundleminjs"
fi

# Remove the orig bundle file
rm "$pubdestjs$bundlejs"

### Copy vendor (already minified) files
find "$pubsrcjs" -type f \( -name "*.min.js" -o -name "*.min.js.map" \) \
  -exec cp {} "$pubdestjs"  \;

