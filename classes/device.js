import Bulb from './devices/bulb';
import Base from './devices/base';

class Device {
  static issue(obj) {
    switch(obj.type) {
      case 'bulb':
        return new Bulb(obj);

      default:
        return new Device(obj);
    }
  }
}

export default Device;
