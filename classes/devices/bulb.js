import {Subject} from "rxjs";
import Base from "./base";
import {State as state} from "../../service/state";
import Config from "../../service/config";
import logger from "../../service/logger";

class Bulb extends Base {
  constructor(obj) {
    super(obj);

    this.confirmObserver = new Subject();
    this.confirmObserver.subscribe((e) => this.chStatConfirm(e));

    this.tryToGetSignals(new Config().setFilename("signals").getData("bulb"));

    // Subscribe on confirmation frim the client
    this.mqtt.addListener(
      "confirm/" + this.id + "/" + this.type + "/switch",
      this.confirmObserver
    );
  }

  chStatConfirm(obj) {
    try {
      if (this.ip === obj.ip && this.mac === obj.mac) {
        logger.info(
          "machine's confirmed changing of the state: " + JSON.stringify(obj)
        );
        state.dispatch({
          type: "UPDATE_STATE_MACHINE",
          payload: {
            id: obj.id,
            update: {timestamp: obj.timestamp, state: obj.state}
          }
        });
      } else {
        throw "/confirm, machine was not registered before";
      }
    } catch (e) {
      logger.error(e + JSON.stringify(obj));
    }
  }
}

export default Bulb;
