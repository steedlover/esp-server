import {Subject} from "rxjs";
import uniqid from "uniqid";
import {State as state} from "../../service/state";
import MqttClient from "../../lib/mqtt-client";
import logger from "../../service/logger";
import he from "he";
import {size} from "underscore";

class Base {
  constructor(obj) {
    this.ip = obj.ip ? obj.ip.toString() : "";
    this.mac = obj.mac ? obj.mac.toString() : "";
    this.type = obj.type ? obj.type.toString() : "";
    this.static_id = obj.static_id ? obj.static_id.toString() : "";
    this.source =
      size(obj.source) > 0 ? he.encode(JSON.stringify(obj.source)) : "";
    this.type_available = false;
    this.error_text = "";
    this.status = 1;
    this.reset_signals = ["new_config", "reset"];

    this.mqtt = new MqttClient().getInstance();
    this.pingObserver = new Subject();
    this.pingObserver.subscribe((e) => this.onPing(e));

    this.register();
  }

  tryToGetSignals(arr) {
    try {
      let defSignal;
      if (typeof arr !== "object" || !arr.length) {
        throw "something wrong with signals array: " + JSON.stringify(arr);
      }
      arr = arr.map((e) => {
        if (e.indexOf(":") >= 0) {
          const miniArr = e.split(":"),
            pureEl = miniArr.filter((e) => e !== "default")[0];
          defSignal = pureEl;
          return pureEl;
        }
        return e;
      });

      this.signals = arr;
      this.signal_default = this.state = defSignal;
      this.type_available = true;
    } catch (e) {
      logger.warn(e);
    }
  }

  sendMqttMessage(signal, message) {
    message = !message || typeof message !== "string" ? "" : message;
    if (
      typeof signal === "string" &&
      (this.signals.indexOf(signal) >= 0 ||
        this.reset_signals.indexOf(signal) >= 0)
    ) {
      this.mqtt.send(this.id + "/" + signal, message);

      // If this is a 'reset' signal, destroy device
      if (this.reset_signals.indexOf(signal) >= 0) {
        this.destroy();
      }
    }
  }

  destroy() {
    state.dispatch({
      type: "REMOVE_MACHINE",
      payload: {
        id: this.id
      }
    });
  }

  onPing(obj) {
    try {
      if (this.ip === obj.ip && this.mac === obj.mac) {
        state.dispatch({
          type: "UPDATE_MACHINE",
          payload: {
            id: obj.id,
            update: {timestamp: obj.timestamp}
          }
        });
      } else {
        throw "/ping, machine was not registered before. ";
      }
    } catch (e) {
      logger.error(e + JSON.stringify(obj));
    }
    return;
  }

  setOffline() {
    state.dispatch({
      type: "MACHINE_GOES_OFFLINE",
      payload: {
        id: this.id,
        update: {
          status: 0,
          state: this.signal_default,
          error_text: "Offline"
        }
      }
    });
    return;
  }

  register() {
    const machines = state.getState().machines;

    function genClientID(t, id) {
      let _t = t && typeof t === "string" ? t : "",
        _id = id && typeof t === "string" ? id : "",
        res = !_id ? uniqid.process(_t ? _t + "_" : "") : _t + "_" + _id;

      if (machines[res] === undefined) {
        return res;
      } else if (!_id) {
        return genClientID(t, id);
      } else {
        return;
      }
    }

    function findMachineByMac(mac) {
      if (!mac) return false;
      const keysArr = Object.keys(machines);
      for (let i = 0; i < keysArr.length; i++) {
        if (
          machines[keysArr[i]].mac === mac &&
          keysArr[i].indexOf("error") < 0
        ) {
          return keysArr[i];
        }
      }
      return false;
    }

    // Check if the machine with this mac address exists
    // if yes, this is the same machine and the same ID should be given
    // but if this mac was not registered with error ID earlier
    const ref = findMachineByMac(this.mac);
    this.id = !ref ? genClientID(this.type, this.static_id) : ref;

    if (!this.id) {
      this.id = genClientID("error");
      this.state = "error";
      logger.error(
        "can't register device. ip:" +
          this.ip +
          ", id:" +
          this.id +
          " (dublicate of static_id: " +
          this.static_id +
          ")"
      );
      this.error_text = "Dublicate of static_id";
    }

    this.mqtt.send(this.ip + "/set_client_id", this.id);
    this.mqtt.addListener("ping/" + this.id, this.pingObserver);

    logger.info(
      "server publish: " + this.ip + "/set_client_id" + ":" + this.id
    );

    return;
  }
}

export default Base;
