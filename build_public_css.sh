#!/bin/sh
pubsrccss='public/src/stylus/';
pubdestcss='public/static/css/'
entrypoint='main.styl';
outputcss='main.css';
outputmincss='main.min.css';
env='production'

if [ -n $1 ]; then
  param="$(echo $1 | cut -d'=' -f1)"
  value="$(echo $1 | cut -d'=' -f2)"

  if [ ! -z "$param" -a "$param" = "env" ]; then
    if [ ! -z "$value" -a "$value" = "dev" ]; then
      env="development"
    fi
  fi
fi

# Check and if needed create the JS folder
if [ -d "$pubdestcss" ]; then
    echo "Using \"$pubdestcss\" folder"
    rm -rf "$pubdestcss"*
else
    mkdir -p "$pubdestcss"
    echo "\"$pubdestcss\" folder has been created"
fi

# Compile CSS file
npx stylus -u autoprefixer-stylus "$pubsrccss$entrypoint" -o "$pubdestcss$outputcss"

if [ "$env" = "production" ]; then
  # Minify CSS file
  npx uglifycss "$pubdestcss$outputcss" --output "$pubdestcss$outputmincss"
  echo "The file has been minified to \"$pubdestcss$outputmincss\""

  # Remove original files (no minified)
  find "$pubdestcss" -type f -name "*.css" -not -name "*.min.css" -exec rm {} \;
fi

# Copy all the css files and min.css
find "$pubsrccss" -type f -name "*.css" -print0 \
  | xargs -0i echo "{}" | xargs -d '\n' -i cp "{}" "$pubdestcss"

if [ "$env" = "development" ]; then
  # If the environment is development, just rename the files without minifying
  find "$pubdestcss" -type f -name "*.css" -not -name "*.min.css" -exec \
    sh -c 'for file do renamed=`echo "$file" | sed "s/\.css/\.min\.css/g"`; cp "$file" "$renamed"; done' sh {} \;
else
  # Mnify all the CSS one by one files in the dest folder
  find "$pubdestcss" -type f -name "*.css" -not -name "*.min.css" -exec \
    sh -c 'for origfile do minfile=`echo "$origfile" | sed "s/\.css/\.min\.css/g"`; npx uglifycss "$origfile" --output "$minfile"; echo "$origfile has been minified successfully"; done' sh {} \;
fi

# Remove all the non minified files
find "$pubdestcss" -type f -name "*.css" -not -name "*.min.css" -exec rm {} \;

