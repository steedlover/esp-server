import path from "path";
import {Subject} from "rxjs";
import {State as state} from "./service/state";
import MqttClient from "./lib/mqtt-client";
import Device from "./classes/device";
import express from "express";
import exphbs from "express-handlebars";
import socket from "./lib/socket-server";
import Config from "./service/config";
import moment from "moment";

let signupObserver = new Subject(),
  mqtt = new MqttClient().getInstance(),
  app = express(),
  server,
  serverConfig = new Config().setFilename("server").getData(),
  io;

mqtt
  .connect()
  .then(() => mqtt.addListener("signup", signupObserver))
  .catch((err) => console.log(err, " , mqtt server connection error"));

signupObserver.subscribe((res) => {
  const dev = new Device.issue(res);
  state.dispatch({
    type: "ADD_MACHINE",
    payload: {
      device: dev,
      id: dev.id
    }
  });
});

// Use Handlebars as layouts
app.engine(".hbs", exphbs({extname: ".hbs"}));
app.set("view engine", ".hbs");

// Use the views folder
app.set("views", path.join(__dirname, "/views"));

// load controllers
import routes from "./lib/routes";
routes(app, {verbose: !module.parent});

// serve static files
app.use(express.static(path.join(__dirname, "public/static")));

app.use(function (err, req, res, next) {
  // log it
  if (!module.parent) console.error(err.stack);

  // error page
  res.status(500).render("5xx", {layout: "default"});
});

//assume 404 since no middleware responded
app.use(function (req, res, next) {
  res.status(404).render("404", {layout: "default", url: req.originalUrl});
});

if (!module.parent) {
  server = app.listen(serverConfig.port, function () {
    console.log("The app listening on port " + serverConfig.port + "!");
  });
  // Open socket connection
  io = new socket().getInstance();
  io.start(server);
}

// Start watching the machines array
let _watch = setInterval(() => {
  let machines = state.getState().machines;
  Object.keys(machines)
    .filter((key) => machines[key].updated && machines[key].status > 0)
    .forEach((id) => {
      const cur = moment().format();
      if (moment(cur).diff(machines[id].updated) > serverConfig.watchInterval) {
        machines[id].setOffline();
      }
    });
}, serverConfig.watchInterval);
